#!/usr/bin/python
import random
import tkinter as tk
from tkinter import ttk

class App(tk.Tk):
    def __init__(self):
        super().__init__()

        self.geometry("400x400")
        self.title("Hello World")
        self.columnconfigure(1, weight=1, minsize=300)

        self.createForm()

        ttk.Button(self, text="Quit", command=self.destroy).grid(column=1,row=2, columnspan=2,sticky=tk.W, padx=5, pady=5)
        ttk.Button(self, text="colour", command=self.colour).grid(column=1,row=3, columnspan=2,sticky=tk.W, padx=5, pady=5)

#        self.displayPhoto()


    def createForm(self):

        username_label = ttk.Label(self, text="Name:")
        username_label.grid(column=0, row=1, sticky=tk.W, padx=10, pady=5)

        self.entry_name = tk.StringVar()
        self.background_colour = "white"
        self.text_colour = "black"
        username_entry = ttk.Entry(self,textvariable=self.entry_name,background=self.background_colour, foreground=self.text_colour)
        username_entry.grid(column=1, row=1, sticky=tk.W, padx=5, pady=5)

        login_button = ttk.Button(self, text="Submit", command=self.submit)
        login_button.grid(column=0, row=2, sticky=tk.W, padx=5, pady=5, columnspan=1)

    def displayPhoto(self):
        self.python_image = tk.PhotoImage(file='./images/test.gif')
        ttk.Label(self,image=self.python_image).grid(column=0,row=5, columnspan=2, padx=5, pady=5)

    def submit(self):
        print("I Love, " + self.entry_name.get())
        if self.entry_name.get() == "phil":
            self.displayPhoto()

    def colour(self):
        colourlist = [
            'black', 'white', 'red', 'green', 'yellow', 'blue', 'brown', 'orange',
            'pink', 'purple', 'grey'
        ]        
        c = random.randint(0, len(colourlist) - 1)
        self.background_colour = colourlist[c]
        self.text_colour = colourlist[c - 1]
        self.config(background=colourlist[c])
#        self.config(foreground=colourlist[c - 1])

# main function - only run at the start
if __name__ == "__main__":
    app = App()
    app.mainloop()
