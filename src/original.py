import random
from tkinter import END, Button, Canvas, Entry, Tk

from PIL import Image, ImageTk

window = Tk()
window.geometry('700x600')

colourlist = [
    'black', 'white', 'red', 'green', 'yellow', 'blue', 'brown', 'orange',
    'pink', 'purple', 'grey'
]


def submit():
  username = entry.get()
  print("I Love, " + username)


#if username == "name":
#showPhoto(window)


def delete():
  entry.delete(0, END)


def colour():
  c = random.randint(0, len(colourlist) - 1)
  entry.config(background=colourlist[c])
  entry.config(foreground=colourlist[c - 1])


colour = Button(window, text="colour", command=colour)
colour.pack(side='right')

delete = Button(window, text="delete", command=delete)
delete.pack(side='right')

submit = Button(window, text="submit", command=submit)
submit.pack(side='right')

entry = Entry()
entry.config(font=('', 50))
entry.config(background='black')
entry.config(foreground='white')
#entry.config(show ='*')
entry.config(width=10)
entry.pack()

#def showPhoto(thisWindow):
#Trying to put image beside the window
canvas = Canvas(window, width=600, height=600)
canvas.pack()

img = (Image.open("./images/test.gif"))
#print(img.size)
#resized_image = img.resize((156, 294))
img = ImageTk.PhotoImage(img)

canvas.create_image(219, 293, image=img)

window.mainloop()